/*
1)while loop

  --while(condtion){
    conditional code
  }

2) do while loop
 do{
   conditional code
 }while(condtion)

3)For loop
  --loop initialization,condtion statement,iteration statment

  a)for(loop initialization;condtion statement;iteration statment){
    statment to be executed if condition statement is ture
  }

  b)for in loop
    for(variableName in object){
      statement to be execute
    }

    c)for of loop

    -for array
        for(variableName of array){
          statement to be execute
        }

    -for object
        for([key,value] of object.entried(obj)){
          statement to be execute
        }

      d)forEach loop

*/

const whileLoop = (count) => {
  while (count <= 10) {
    console.log(count)
    count++
  }
}

const doWhileLoop = (count) => {
  do {
    console.log(count)
    count++
  }
  while (count <= 10)
}



const forLoop = () => {
  for (let count = 0; count <= 10; count++) {
    console.log(count)
  }
}


const forInLoop = () => {
  let gradeObj = { excellent: 'A', good: 'B', notBad: 'C', bad: 'D' }
  console.log(gradeObj)

  for (grade in gradeObj) {
    if (grade == 'notBad') {
      continue
    }
    else
      console.log(grade)
  }
}


const forOfLoopObj = () => {
  let gradeObj = { excellent: 'A', good: 'B', notBad: 'C', bad: 'D' }

  for ([key, grade] of Object.entries(gradeObj)) {
    console.log(key + ': ' + grade)
  }
}

const forOfLoopArray = () => {
  let gradeList = [
    { excellent: 'A', good: 'B', notBad: 'C', bad: 'D' },
    { excellent: 'E', good: 'F', notBad: 'F', bad: 'G' }
  ]

  console.log(gradeList)
  for (grade of gradeList) {
    console.log(grade)
    for (key in grade) {
      console.log(key)
    }
  }

  for (grade in gradeList) {
    console.log(grade)
  }
}


const forEachLoopArray = () => {
  let gradeList = [
    { excellent: 'A', good: 'B', notBad: 'C', bad: 'D' },
    { excellent: 'E', good: 'F', notBad: 'F', bad: 'G' }
  ]

  gradeList.forEach((grade) => {
    console.log(grade)
  })
}


//whileLoop(1)
//doWhileLoop(1)
//forInLoop()
//forOfLoop()
//forOfLoopArray()