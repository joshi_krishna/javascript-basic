
//table 1
const orderMomo = () => 'Momo is ready'

//table 2
const orderChaumin = () => 'Chaumin is ready'

//table 3
const orderPizza = () => {
  let pizza = true
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (pizza)
        resolve({ id: 123, name: 'pizza' })
      else
        reject('pizza is not ready! should not pay')
    }, 5)
  })
 
}

//make payment
const makePayment = (item) => item + ' payment is done'

//make order
const order = async ()=> {
  let payment = null
 // --------------------------------------
  let momo = orderMomo()
  console.log(momo)
  payment = makePayment('Momo')
  console.log(payment)
//----------------------------------------


  let chaumin = orderChaumin()
  console.log(chaumin)
  payement = makePayment('chaumin')
  console.log(payment)

  //--------------.then.catch-----------------------
  orderPizza().then((pizza) => {
    console.log(pizza)
    payment = makePayment('pizza')
    console.log(payment)
  }).catch((error) => {
    console.log(error)
  })

  //------------------async-await----------------------------
  try {
    let pizza = await orderPizza()
    document.getElementById('payment').innerHTML = pizza.id
  }
  catch (err) {
    alert(err)
  }
  payment = makePayment('pizza')
  console.log(payment)
  //document.getElementById('payment').innerHTML = payment
}
//------------------------------------
order()

//promise state
//1)pending -- still working
//2)fulfilled -- success
//3)rejected -- error

//1)->callback --callback hell
 //2) --Promise ---
 ///3)async and await 

