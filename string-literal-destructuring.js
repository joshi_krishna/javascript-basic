///string template(Es6)
const getName = (firstName, middleName, lastName, adddress) => {

  //old method
  const fullName = firstName + ' ' + middleName + ' ' + lastName
  let text1 = 'my name is ' + fullName + '.  I live in ' + adddress

  //new method
  const fullName = `my full name is ${firstName} ${middleName} ${lastName}`
  let text = ` my name is ${fullName}.I live in ${adddress} `
  return text
}

// const fullName = getName('Krishna', 'Raj', 'Joshi')
// alert(fullName)
//---------------------------------------------------------------------------------------

//--------------------------destructuring----------------------------------


const destrucureArray = () => {
  const myArray = [1, 2, 3, 4]
  const name = ['Krishna', 'Raj', 'joshi']
  //console.log(myArray)
  //console.log(myArray[3])
  let [a, b, c, d] = myArray
  console.log(a)
  console.log(b)
  console.log(c)
  console.log(d)

  //one method to get value
  // let firstName = name[0]
  // let middleName = name[1]
  // let lastName = name[2]

  //next method
  let [firstName, middleName, lastName] = name
  console.log(firstName)
  console.log(middleName)
  console.log(lastName)
  let fullName = `${firstName} ${middleName} ${lastName}`
  console.log(fullName)


  const coins = [1, 2, 3, 4, 5, 6, 7, 8]
  let [coin1, coin2, ...xyz] = coins
  console.log(coin1)
  console.log(coin2)
  console.log(xyz)

}

const destrucureObject = () => {
  let myObj = { a: 10, b: 12, c: 13 }
  let myName = { fName: 'krisha', mName: 'raj', lName: 'joshi' }
  console.log(myObj.a)
  console.log(myObj.b)
  console.log(myObj.c)

  const { a, b, c } = myObj
  const { fName, mName, lName } = myName

  console.log(a)
  console.log(b)
  console.log(c)

  console.log(fName)
  console.log(mName)
  console.log(lName)

  const coins = { x: 1, y: 2, z: 3, d: 4, e: 5, f: 6 }
  const { x, y, ...rest } = coins

  console.log(a)
  console.log(b)
  console.log(rest)

}


destrucureObject()