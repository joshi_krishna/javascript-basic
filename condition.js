/*

1)If-else
 -if statement
   -multiple statements
      if(expression){
        statement to be executed if expression is true
      }
  -single statement
      if(expression)statement to be executed if expression is true
 -if else statment
    -multiple statements
      if(expression){
        statement to be executed if expression is true
      }
      else{
         statement to be executed if expression is false
      }
  -single statement
       if(expression)statement to be executed if expression is true
       else statement to be executed if expression is false
      


 -if else if statement

  if(expression 1){
        statement to be executed if expression 1 true
      }
  else if(expression 2){
      statement to be executed if expression 2 is true
  }
   else if(expression 3){
      statement to be executed if expression 3 is true
  }
  else{
    statement to be executed if no expression it true
  }

  -Switch Case
    switch(expression){
      case condition 1:statement1
      break
      case condtion 2 :statement 2
      break
      case condtion 3 :statement 3
      default:default statement
    }

*/

const Ifcondition = (param) => {
  if (!param) return 'yes'

  if (param) {
    param = false
    return 'no'
  }
}


const IfElsecondition = (param) => {
  if (param == 1) return 'I am 1'
  else return 'I am not 1'
//--------------------------------------------
  if (param === 1) {
    param = param + 1
    return 'I am 2'
  }
  else {
    param = param - 1
    return 'I am not 2'
  }
}




const IfElseIfcondition = (param) => {
  if (param == 'A') {
    return 'Excellent'
  }
  else if (param == 'B') {
    return 'Good'
  }
  else if (param == 'C') {
    return 'Not bad'
  }
  else {
    return 'Bad'
  }
}


const switcCase = (grade) => {

  switch (grade) {
    case 'A':
      return 'Excellent'
    case 'B':
      return 'Good'
    case 'C':
      return 'Not Bad'
    default:
      return 'Bad'
  }
}

// const result = Ifcondition(true)
// console.log('result',result)

// const result = IfElsecondition('122')
// console.log('result',result)

// const result = IfElseIfcondition()
// console.log('result',result)

const result = switcCase('')

document.getElementById('result').innerHTML = result
