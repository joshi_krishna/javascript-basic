class Mammal {

  constructor(name) {
    this.name = name
  }

  eats() {
    return `${this.name} eats Food`
  }
}



class Dog extends Mammal {

  constructor(name, owner) {
    super(name)
    this.owner = owner
  }

  getDog() {
    return `This is ${this.owner}'s dog.Name:${this.name}`
  }

  static eats() {
    return `${this.name} eats chiken`
  }
}

// let mammal = new Mammal('Leo')
// let result = mammal.eats()
// console.log(result)
// document.getElementById('result').innerHTML = result


let dog = new Dog('Leo','Devid')
let result2 = Dog.eats()
document.getElementById('result').innerHTML = result2


class Student {
  constructor(name, address, age, roleNumber) {
    this.name = name
    this.address = address
    this.age = age
    this.roleNumber = roleNumber
  }
 
  getStudent() {
    return `My name is ${this.name}. I live in ${this.address}. My role number: ${this.roleNumber} and my age: ${this.age}`
  }

}


class Player extends Student{
  constructor(name, address, age, roleNumber, game){
    super(name, address, age, roleNumber)
    this.game = game
  }

  getPlayer(){
    return `${this.name} is player of ${this.game}`
  }
}

// let player = new Player('Krishna', 'Kathmandu', 30, 5, 'criket')
// document.getElementById('result').innerHTML = player.getPlayer()

// document.getElementById('result1').innerHTML = player.getStudent()


//static function
class A {
  static getA() {
    return 'I am A'
  }
}

document.getElementById('result1').innerHTML = A.getA()
