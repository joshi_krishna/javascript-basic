//javascript ES5 function
//debugging
//variable
//comments
//1)single line Comment
/*2)multiple line Comments*/

//display data
//let name = 'krishna'
function display(data) {
  // console.log('data in display',data)
  document.getElementById('name').innerHTML = data.name
  document.getElementById('age').innerHTML = data.age
  document.getElementById('number').innerHTML = data.number
}

//return data
function getEmp() {
  const age = '23'
  const number = '1224456789'
  if(age){
    var name = "jackson rai"
    console.log("inside if", name)
    console.log('age', age)
  }

  console.log('outside if',name)
  return { name, age, number }
}


function main(){
  const data = getEmp()
  display(data)
}

main()

//variable -  var,let ,const
/*scope of varibale
    1)Block scope {}
    2)Function scope
    3)Global scope
    */

