
//array
let employeeList = []

//object
let employee = { name: 'krishna', address: 'kathmandu', city: 'ktm' }

const fetchEmployee = () => {
  employee.phoneNumber = '1234567890'
  return employee
}


const fetchEmployeeList = () => {
  return employeeList
}


const main = () => {
  const emp = fetchEmployee()
  const empList = fetchEmployeeList()

  //--------object section--------------------
  console.log(emp)

  //---for key and value of object
  for (const [key, value] of Object.entries(emp)) {
    console.log(key + ':' + value)
    document.getElementById('name').innerHTML = key + ':' + value
  }

  //--for key only
  for(item in emp){
    console.log(item)
  }


  //---------array section--------------------------------------------------
  empList.push(emp)
  emp.email = 'test@gmail.com'
  empList.push(emp)
  console.log(empList)

  //----------------------------------------
  for (item of empList) {
    console.log(item)
    document.getElementById('name').innerHTML = item.name
  }

  const fun = (item) => {
    console.log('inside foreach')
    console.log(item)
    document.getElementById('name').innerHTML = item.name
  }

  empList.forEach(fun)

// //--------------------------------------------------------
  for (let i = 0; i < empList.length; i++) {
    console.log(i)
    console.log(empList[i])
  }

  for (item in empList) {
    console.log(item)
  }


//-----single values array-----
  let coins = [1, 2, 3, 4, 5, 6]
  console.log(coins.pop()) //remove last item from array
  coins.push(7)
  coins.unshift(45)
  console.log(coins)

  //-----------------------------------------------------
  let reduce = coins.reduce((item1, item2) => {
    return item1 + item2
  })


  
//---------------------------------------

  const predicate = (coin) => {
    return coin < 5
  }
  let fil = coins.filter(predicate)

  console.log('reduce', reduce)
  console.log('fil', fil)
  console.log('reverse', fil.reverse())

 }


let names = ['b', 'b', 'b']
 let every = names.every((item) => {
  return item == 'b'
})

console.log('every',every)

let some = names.some((item) => {
  return item == 'b'
})
console.log('some',some)
main()


